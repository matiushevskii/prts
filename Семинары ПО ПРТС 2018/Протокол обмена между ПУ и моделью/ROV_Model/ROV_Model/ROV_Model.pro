QT += core network widgets
QT -= gui

CONFIG += c++11

TARGET = model
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  kx_protocol.cpp \
            main.cpp \
            rov_model.cpp \
            qkx_coeffs.cpp \
            qpiconfig.cpp \
            pult_protocol.cpp \
    su_rov.cpp

HEADERS +=  kx_protocol.h \
            rov_model.h \
            qkx_coeffs.h \
            qpiconfig.h \
            pult_protocol.h \
    su_rov.h

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
