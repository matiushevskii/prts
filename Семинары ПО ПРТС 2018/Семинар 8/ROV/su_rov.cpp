#include "su_rov.h"

SU_ROV::SU_ROV(QObject *parent) : QObject(parent)
{
    K_Protocol = new Qkx_coeffs("protocols.conf", "ki");
    X_Protocol = new x_protocol ("protocols.conf","xi",X);
    model = new ROV_Model ();
    if (K[1]>0) T=K[1];
    else T=0.01;
    time.start(T*1000);//запуск проводим в мсек
    connect (&time, SIGNAL(timeout()), SLOT(tick()));

}



void SU_ROV::tick()
{
    get_data_from_model();
    Control_Kurs();
    BFS_DRK(X[49][0], 0,0,0);
    model->tick(X[27][0], X[28][0], X[29][0], X[30][0],T);
}





void SU_ROV::addKurs()
{
    X[1][0]++;
}
void SU_ROV::decKurs(){
    X[1][0]--;
}

void SU_ROV::changeMode(int btn, bool state)
{
    if (btn==Automatiz && state) mode=Automatiz;
    else mode=Ruchnoi;
}



void SU_ROV::BFS_DRK(double Upsi, double Uteta, double Ugamma, double Ux){
    //ограничим входные задающие сигналы в бфс ДРК
    X[11][0] = saturation(Ux, K[11]);
    X[12][0] = saturation(Upsi,K[12]);
    X[13][0] = saturation(Uteta,K[13]);
    X[14][0] = saturation(Ugamma, K[14]);

    //далее по структурной схеме БФС, вычисляем значения после первого сумматора
    X[15][0] = -X[11][0] - X[12][0]; //промежуточное значение для ВМА МВЛ (управление курсом и маршем)
    X[16][0] = X[11][0] + X[12][0]; //промежуточное значение для ВМА МНЛ (управление курсом и маршем)
    X[17][0] = X[11][0] - X[12][0]; //промежуточное значение для ВМА МВП (управление курсом и маршем)
    X[18][0] = -X[11][0] + X[12][0]; //промежуточное значение для ВМА МНП (управление курсом и маршем)

    //далее по структурной схеме БФС, вычисляем значения после второго сумматора
    X[19][0] = X[15][0] + X[13][0];
    X[20][0] = X[16][0] + X[13][0];
    X[21][0] = X[17][0] - X[13][0];
    X[22][0] = X[18][0] - X[13][0];

    //далее по структурной схеме БФС, вычисляем значения после третьего сумматора
    X[23][0] = X[19][0] + X[14][0];
    X[24][0] = X[20][0] + X[14][0];
    X[25][0] = X[21][0] + X[14][0];
    X[26][0] = X[22][0] + X[14][0];

    //ограничим и промасштабируем управляющие значения напряжений для ВМА
    X[27][0] = saturation(X[23][0],K[23])*K[27]; //управляющее напряжение на ВМА МВЛ
    X[28][0] = saturation(X[24][0],K[24])*K[28]; //управляющее напряжение на ВМА МНЛ
    X[29][0] = saturation(X[25][0],K[25])*K[29]; //управляющее напряжение на ВМА МВП
    X[30][0] = saturation(X[26][0],K[26])*K[30]; //управляющее напряжение на ВМА МНП
}

void SU_ROV::Control_Kurs(){
    if (mode==Ruchnoi) {
        X[40][0]=X[1][0];
        X[47][0]=X[40][0]*K[40];
        X[48][0]=X[47][0];
        X[49][0]=saturation(X[48][0],K[49]);
    }
    else if (mode==Automatiz) {
        X[41][0]=X[1][0];
        X[43][0]=X[41][0]-X[42][0];
        X[44][0]=X[43][0]*K[44];

        X[45][0]=X[50][0]*K[45];
        X[46][0]=X[44][0]-X[45][0]+X[41][0]*K[43];
        X[48][0]=X[46][0];
        X[49][0]=saturation(X[48][0],K[49]);
    }
}

float SU_ROV::saturation(float input, float max) {
    if (fabs(input)>= max)
        return (sign(input)*max);
    else return input;
}


void SU_ROV::get_data_from_model(){
    X[32][0]=model->Fx;
    X[33][0]=model->vx_global;
    X[34][0]=model->vx_local;
    X[35][0]=model->x_global;
    X[36][0]=model->y_global;
    X[37][0]=model->z_global;
    X[39][0]=model->Tetta_g;
    X[40][0]=model->Gamma_g;
    X[42][0]=model->Psi_g;
    X[50][0]=model->W_Psi_g;
}
