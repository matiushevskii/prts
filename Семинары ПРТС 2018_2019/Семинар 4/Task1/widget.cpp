#include "widget.h"


Widget::Widget(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    connect(horizontalSlider, SIGNAL(valueChanged(int)),
            progressBar, SLOT(setValue(int)));
}

Widget::~Widget()
{

}
